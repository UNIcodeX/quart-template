Quart Template
==============

*This is a template for a Quart based web application*

# Installing and Configuring
1. Ensure that your machine has Python 3.7 or later installed.
2. Ensure that virtualenv is installed.
3. Clone or download / extract this repository onto your system.
4. cd into the repository.
5. Run `virtualenv --python=python3.7 venv`
6. Issue `source venv/bin/activate` to activate the virtual environment.
7. Ensure that your machine has a static IP address and that a DNS entry has been made, pointing to that IP.
8. Ensure that ports 80 and 443 have been opened and forwarded on your firewall.
9. Update the `run_production` script, where applicable, to point to the location of your certificate and private key.
10. Test the server by issuing `./run_production` in the repository's base directory and navigating to the site at https://{{ FQDN }}
11. Ensure that Nginx is installed and configure /etc/nginx/sites-available/default, using the supplied `nginx-default-https-{{ type }}` files found in the `server_config` directory.
    *  Nginx will only be used to redirect HTTP requests to HTTPS. 
12. Start Nginx and navigate to http://{{ FQDN }}. The page should redirect to the secure site.
13. Add an entry to the root cron to renew the certificate, using `sudo crontab -e -u root`
    *  Add the following line:
        >  `00 00 * * * certbot renew -n --pre-hook "supervisorctl stop tas-technical-web" --post-hook "supervisorctl start tas-technical-web"`
    *  Save and close the file

**NOTE:** If you do not have an SSL certificate, follow the instructions at https://certbot.eff.org, 
depending on your system configuration.
- Use the cert.pem and privkey.pem to run Hypercorn as a supervisor service.

