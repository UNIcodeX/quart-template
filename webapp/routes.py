from quart import render_template, redirect, url_for, request, flash, session, make_response
from webapp import app, db, bcrypt, login_manager
from webapp.forms import LoginForm
from webapp.models import User, Post
from flask_login import login_user, current_user, logout_user, login_required


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('login', next=request.url))


@app.route('/')
@login_required
async def index():
    result = await render_template('index.html')
    response = await make_response(result)
    response.push_promises.update(
        [url_for('static', filename='css/bulma.min.css'),
         url_for('static', filename='js/jquery.min.js'),
         url_for('static', filename='js/fontawesome/all.js'),
         url_for('static', filename='img/favicon-16x16.png'),
         url_for('static', filename='img/favicon-32x32.png'),
         url_for('static', filename='img/favicon.ico')
        ]
    )
    return response


@app.route('/test/')
@login_required
def test():
    return 'test'


@app.route('/login/', methods=['GET', 'POST'])
async def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next = request.args.get('next')
            if next:
                return redirect(next)
            else:
                return redirect(url_for('index'))
        else:
            await flash('Login unsuccessful. Check username and password.', 'danger')
    return await render_template('login.html', title='Login', form=form)


@app.route('/logout/')
@login_required
def logout():
    if current_user.is_authenticated:
        logout_user()
    return redirect(url_for('login'))

